package handler

import (
	proto "bitbucket.org/userapidemo/phone/pkg/generated/api"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestPhone(t *testing.T) {

	Convey("Given expected data for CreatePhone", t, func() {
		req := &proto.PhoneRequest{UserId: 1, Phone: "123456", Type: "cell"}
		res := &proto.PhoneResponse{}
		resStatus := "success"
		Convey("When calling rpc endpoint for CreatePhone", func() {
			Convey("Then rpc endpoint error should be nil", func() {
				So(err, ShouldBeNil)
			})
			Convey("Then rpc endpoint response status should be 'success' ", func() {
				So(res.Status, ShouldEqual, resStatus)
			})
		})
	})
}
