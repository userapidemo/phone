package handler

import (
	"bitbucket.org/userapidemo/phone/internal/data"
	"bitbucket.org/userapidemo/phone/internal/model"
	proto "bitbucket.org/userapidemo/phone/pkg/generated/api"
	"context"
	"fmt"
	"github.com/asim/go-micro/v3/util/log"
	"github.com/jmoiron/sqlx"
)

type handler struct {
	svc data.PhoneRepo
}

func NewPhoneService(conn *sqlx.DB) proto.PhoneServiceHandler {
	return &handler{
		svc: data.NewDataService(conn),
	}
}

func (h *handler) CreatePhone(ctx context.Context, req *proto.PhoneRequest, res *proto.PhoneResponse) error {
	phone := &model.Phone{UserId: req.UserId, Phone: req.Phone, PhoneType: req.Type}
	id, err := h.svc.CreatePhone(ctx, phone)
	if err != nil {
		fmt.Println(err)
		log.Fatal("Didn't able to create the phone: ")
		res.Status = "failed"
		return nil
	}
	res.PhoneId = id
	res.Status = "success"
	return nil
}
