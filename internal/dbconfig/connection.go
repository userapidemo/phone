package dbconfig

import (
	"fmt"
	"github.com/asim/go-micro/v3/util/log"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/newrelic/go-agent/_integrations/nrpq"
)

func GetConnSQLX() *sqlx.DB {
	const connStr = "user=%s password=%s dbname=%s host=%s port=%d sslmode=disable"
	ds := fmt.Sprintf(connStr,
		"postgres",
		"12345",
		"userapi",
		"postgresqldb",
		5432)

	db, err := sqlx.Open("nrpostgres", ds)
	if err != nil {
		log.Info("Didn't able to make db connection: ", err)
		return nil
	}
	return db

}
