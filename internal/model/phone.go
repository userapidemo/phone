package model

type Phone struct {
	Id        int32  `db:"id"`
	UserId    int32  `db:"user_id"`
	Phone     string `db:"phone"`
	PhoneType string `db:"phone_type"`
}
