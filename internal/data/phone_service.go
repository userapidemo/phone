package data

import (
	"bitbucket.org/userapidemo/phone/internal/model"
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type PhoneRepo interface {
	CreatePhone(ctx context.Context, request *model.Phone) (int32, error)
}

type phone struct {
	conn *sqlx.DB
}

func NewDataService(db *sqlx.DB) PhoneRepo {
	return &phone{
		conn: db,
	}
}

func (u *phone) CreatePhone(ctx context.Context, request *model.Phone) (int32, error) {
	tx := u.conn.MustBegin()
	var id int32
	err := tx.QueryRowx("INSERT INTO phone_info.phone (user_id,phone,p_type) VALUES ($1,$2,$3) returning id",
		request.UserId, request.Phone, request.PhoneType).Scan(&id)

	if err != nil {
		tx.Rollback()
		return 0, errors.Wrap(err, "phone insert error")
	}
	tx.Commit()
	return id, err
}
