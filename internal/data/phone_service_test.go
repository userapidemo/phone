package data

import (
	"bitbucket.org/userapidemo/phone/internal/dbconfig"
	"bitbucket.org/userapidemo/phone/internal/model"
	"context"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestCreatePhone(t *testing.T) {
	Convey("Given phone information to save into DB ", t, func() {
		samples := model.Phone{
			UserId:    1,
			Phone:     "12345",
			PhoneType: "cell0",
		}
		dataSrv := NewDataService(dbconfig.GetConnSQLX())
		Convey("When calling CreatePhone method", func() {
			id, err := dataSrv.CreatePhone(context.Background(), &samples)
			Convey("Then the method should not return error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Then the method should return a id which is not zero", func() {
				So(id, ShouldNotEqual, 0)
			})
		})
	})
}
