package main

import (
	proto "bitbucket.org/userapidemo/phone/pkg/generated/api"
	"context"
	"fmt"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/util/log"
)

func main() {
	client := micro.NewService()
	service := proto.NewPhoneService("sf.phone.service", client.Client())
	res, err := service.CreatePhone(context.Background(), &proto.PhoneRequest{
		UserId: 3,
		Phone:  "213452345234",
		Type:   "cell", //land
	})
	if err != nil {
		log.Fatal("Got error ==> CreatePhone: ", err)
	}
	fmt.Println(res.PhoneId)
	fmt.Println(res.Status)
}
