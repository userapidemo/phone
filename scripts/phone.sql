CREATE SCHEMA phone_info
CREATE TABLE phone_info.phone (
                                  id         serial,
                                  user_id    int NOT NULL ,
                                  phone 	 varchar(20),
                                  p_type   	 varchar(20),
                                  PRIMARY KEY (id)
)