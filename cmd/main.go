package main

import (
	"bitbucket.org/userapidemo/phone/internal/dbconfig"
	"bitbucket.org/userapidemo/phone/internal/handler"
	"bitbucket.org/userapidemo/phone/pkg/generated/api"
	"github.com/asim/go-micro/v3"
	"github.com/pkg/errors"
	"log"
	"os"
)

func main() {
	service := micro.NewService(
		micro.Name("sf.phone.service"),
		micro.Address(":8002"),
	)
	service.Init()

	dbconn := dbconfig.GetConnSQLX()
	h := handler.NewPhoneService(dbconn)

	api.RegisterPhoneServiceHandler(service.Server(), h)

	if err := service.Run(); err != nil {
		log.Fatalln(nil, errors.Wrap(err, "unable to run service"))
		os.Exit(0)
	}

	os.Exit(0)

}
